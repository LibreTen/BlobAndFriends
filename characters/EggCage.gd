extends Area2D

export var id = 0

func _ready():
	$Egg.frame = id


func _on_EggCage_body_entered(body):
	if body.name == "Player":
		find_parent("Level").add_follower(id)
		find_parent("Level").first_egg()
		queue_free()
