extends Node2D

#var min_x = 24
#var min_y = 24
#var max_x = 1576
#var max_y = 876

var target : KinematicBody2D
var sprite_id := 0
var active = false

func _process(delta):
	if active:
		if target.state != target.s.STAY:
			hide()
			return
		show()
		var camera :Camera2D = get_parent()
		
		var min_x = -get_viewport_rect().end.x/2 * camera.zoom.x +24
		var min_y = -get_viewport_rect().end.y/2 * camera.zoom.y +24
		var max_x = get_viewport_rect().end.x/2* camera.zoom.x -24
		var max_y = get_viewport_rect().end.y/2* camera.zoom.y -24
		
		
		
		var target_pos = target.position
		var player_pos = find_parent("Player").position
		var pos : Vector2 = (target_pos - camera.global_position)
		
		$Sprite.look_at(target_pos)
		
		pos.x = clamp(pos.x,min_x,max_x)
		pos.y = clamp(pos.y,min_y,max_y)
		position = pos


func enable():
	show()
	$Egg.frame = sprite_id
	active = true

func disable():
	hide()
	active = false


func _on_Area2D_mouse_entered():
	target.cursor_hover = true
	if target.state == target.s.STAY:
		$Egg.modulate = Color.green


func _on_Area2D_mouse_exited():
	target.cursor_hover = false
	$Egg.modulate = Color.white
