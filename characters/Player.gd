extends KinematicBody2D

var speed = 200
var movy = 0
onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")*4
var jump_force = -300

var terminal_speed = 1000
var can_respawn = true
var died = false

func _input(event):
	if event.is_action_pressed("throw"):
		throw()
	elif event.is_action_pressed("throw2"):
		throw2()
	elif event.is_action_pressed("respawn") && can_respawn:
		position = Vector2(-198,612)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var movx = 0
	if !died:
		movx = Input.get_action_strength("right") - Input.get_action_strength("left")
	
	var snap
	
	if is_on_floor():
		if movx == 0:
			$AnimatedSprite.play("idle")
		else:
			$AnimatedSprite.play("walk")
		
		
		if Input.is_action_pressed("jump") && !died:
			$jump.play()
			movy = jump_force
			snap = Vector2.ZERO
		else:
			movy = 0
			snap = Vector2.DOWN*16
	else:
		if movy < terminal_speed:
			movy += gravity*delta
		snap = Vector2.DOWN*16
		if movy < 0:
			$AnimatedSprite.play("jump_up")
		else:
			$AnimatedSprite.play("jump_down")
	
	if get_global_mouse_position() > position:
		$AnimatedSprite.flip_h = false
	else:
		$AnimatedSprite.flip_h = true
	
	
	var movement = Vector2(movx*speed,movy)
	
	move_and_slide_with_snap(movement,snap,Vector2.UP)
	#move_and_slide(movement*delta,Vector2.UP)
	

func throw():
	if died: return
	var fly_dir = get_global_mouse_position()-position
	get_parent().throw(fly_dir,false)

func throw2():
	if died: return
	var fly_dir = get_global_mouse_position()-position
	get_parent().throw(fly_dir,true)

func die():
	died = true
	$AnimatedSprite.flip_v = true


func _on_CameraCollision_body_entered(body):
	pass # Replace with function body.
