extends KinematicBody2D

var camera: Camera2D
var active = false
var speed = 90
var movy = 0
var movx = 0
var gravity = 150
var jump_force = -100
var died = false
var camera_active = true

var ff = true

#func _input(event):
#	if event.is_action_pressed("throw"):
#		$Particles2D.restart()
#		#explode()

func setup(c):
	camera = c
	camera.limit_left = global_position.x - $Sprite.texture.get_size().x/2
	$AnimationPlayer.play("default")

func _physics_process(delta):
	if active:
		print(is_on_floor())
		if is_on_floor() && ff:
			$fall.play()
			ff = false
			$Timer.start()
			movx = 0
			$Particles2D.restart()
		elif !is_on_floor():
			ff = true
			movy += gravity*delta
			movx = speed
		move_and_slide(Vector2(movx,movy),Vector2.UP)

func _process(delta):
	if camera != null && camera_active:
		camera.limit_left = global_position.x - $Sprite.texture.get_size().x/2


func _on_Timer_timeout():
	movy = jump_force
	$hop.play()


func _on_die_collision_body_entered(body):
	if died:
		return
	$die.play()
	$died.start()
	died = true
	find_parent("Level").died()

func _on_died_timeout():
	get_tree().reload_current_scene()


func _on_AnimationPlayer_animation_finished(anim_name):
	active = true
	movy = jump_force

func died():
	$me_died.play()
	

func stop_camera():
	camera_active = false
