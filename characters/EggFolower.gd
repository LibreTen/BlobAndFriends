extends KinematicBody2D

enum s {
	IDLE,
	FOLLOWING,
	FLYING,
	FALLING,
	STAY,
	TELEPORTING
}

const GRAVITY = 12000
const JUMP_FORCE = -3200

const HARD_CAP = 320

var target : Node2D
var max_distance = 20
var min_distance = 16

var state = s.IDLE

var speed = 100
var movy = 0
var gravity = 12000
var jump_force = -2000

var direction : Vector2
var distance = 0

var fly_dir : Vector2
var fly_speed = 5


var stay_flag = false
var cursor_hover = false

var frame_id

func set_sprite(id):
	frame_id = id
	$Sprite.frame = id

#func _input_event(viewport, event, shape_idx):
#	print(event)

func _physics_process(delta):
	if target == null:
		return
	direction = target.position - position
	distance = direction.length()
	
	
	match state:
		s.IDLE:
			idle()
		s.FOLLOWING:
			following(delta)
		s.FLYING:
			flying(delta)
		s.FALLING:
			falling(delta)
		s.STAY:
			stay(delta)
		s.TELEPORTING:
			teleporting(delta)
	


func idle():
	if distance > max_distance:
		state = s.FOLLOWING
	if distance > HARD_CAP:
		position = target.position
	
	move_and_slide_with_snap(Vector2.ZERO,Vector2.DOWN*16)

func following(delta):
	if distance > HARD_CAP:
		collision_layer = 0
		state = s.TELEPORTING
	
	elif is_on_floor() and distance < min_distance:
		state = s.IDLE
	else:
		var movx = direction.x
		
		var snap = Vector2.ZERO
		
		if is_on_floor():
			jump_force = JUMP_FORCE+direction.y*100
			if -jump_force >= gravity*delta:#target.position.y < position.y+16:
				$hop.pitch_scale = rand_range(2,4)
				$hop.play()
			movy = jump_force
			
			
			#gravity = GRAVITY-direction.y*200
			#gravity = clamp(gravity,30000,60000)
		else:
			movy += gravity*delta
			snap = Vector2.DOWN*16
		
		if movx > 0:
			$Sprite.flip_h = false
		elif movx<0:
			$Sprite.flip_h = true
		
		var movement = Vector2(movx*speed,movy)
		move_and_slide_with_snap(movement*delta,snap,Vector2.UP)
		
		
	

func throw(d,sflag):
	collision_layer = 17
	stay_flag = sflag
	$Timer.start()
	state = s.FLYING
	position = target.position
	fly_dir = d
	fly_dir *= fly_speed
	fly_dir = fly_dir.clamped(500)
	$throw.pitch_scale = rand_range(2,4)
	$throw.play()

func flying(delta):
	fly_dir.y += GRAVITY*delta/30
	move_and_slide(fly_dir)
	
	if fly_dir.x > 0:
		$Sprite.flip_h = false
	elif fly_dir.x<0:
		$Sprite.flip_h = true
	
	if get_slide_count() > 0:
		for i in get_slide_count():
			var body  =get_slide_collision(i).collider
			if body.is_in_group("breakable"):
				body.trigger()
		state = s.FALLING
		$oof.play()
		movy = 0
	

func _process(delta):
	if state == s.FLYING && get_slide_count() > 0:
		for i in get_slide_count():
			var body  =get_slide_collision(i).collider
			if body.is_in_group("breakable"):
				body.trigger()
		state = s.FALLING
		movy = 0

func falling(delta):
	if is_on_floor():
		$Timer.stop()
		if !stay_flag:
			state = s.FOLLOWING
			find_parent("Level").return_to_list(self)
		else:
			state = s.STAY
			if cursor_hover:
				modulate = Color.green
			else:
				modulate = Color.lightblue
		
	else:
		movy += GRAVITY*delta
	
	var movement = Vector2(0,movy)
	move_and_slide(movement*delta,Vector2.UP)

func teleporting(delta):
	position += (target.position - position)*delta
	if distance < min_distance:
		collision_layer = 17
		state = s.FOLLOWING

func stay(delta):
	if is_on_floor():
		movy = 0
	else:
		movy += GRAVITY *delta
	move_and_slide_with_snap(Vector2(0,movy)*delta,Vector2.DOWN*16,Vector2.UP)
	

func retrieve():
	state = s.FOLLOWING
	modulate = Color.white
	find_parent("Level").return_to_list(self)


func _on_Timer_timeout():
	state = s.FOLLOWING
	find_parent("Level").return_to_list(self)


func _on_Area2D_mouse_entered():
	cursor_hover = true
	if state == s.STAY:
		modulate = Color.green


func _on_Area2D_mouse_exited():
	cursor_hover = false
	if state == s.STAY:
		modulate = Color.lightblue
	else:
		modulate = Color.white


