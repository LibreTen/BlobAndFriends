extends Node2D

var egg_base = preload("res://characters/EggFolower.tscn")
var radar_base = preload("res://characters/EggRadar.tscn")
#var run_base = preload("res://Run.tscn")

export var run = false

func _input(event):
	if event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	elif event.is_action_pressed("retrieve"):
		var children = $Thrown.get_children()
		children.invert()
		for u in children:
			if u.cursor_hover:
				u.retrieve()
				break

func _ready():
	if run:
		$Player.position = Vector2(0,-1300)
		$Player.can_respawn = false
		$Monster.setup($Player/Camera2D)
		add_follower(0)
		add_follower(1)
		add_follower(2)
		add_follower(3)
		add_follower(4)




func add_follower(id):
	var egg = egg_base.instance()
	
	var children = $EggFollowers.get_children()
	if children.empty():
		egg.target = $Player
	else:
		egg.target = children.back()
	$EggFollowers.add_child(egg)
	egg.set_sprite(id)
	egg.position = egg.target.position
	$sounds/saved.play()

func throw(fly_dir,sflag):
	if $EggFollowers.get_child_count() >0:
		var egg = $EggFollowers.get_children().front()
		$EggFollowers.remove_child(egg)
		$Thrown.add_child(egg)
		if $EggFollowers.get_child_count() >0:
			$EggFollowers.get_children().front().target = $Player
		
		egg.throw(fly_dir,sflag)
		

func return_to_list(egg : KinematicBody2D):
	var children = $EggFollowers.get_children()
	if children.empty():
		egg.target = $Player
	else:
		egg.target = children.back()
	egg.get_parent().remove_child(egg)
	$EggFollowers.add_child(egg)

func activate(id):
	var u : Node2D = get_node("Objects/"+str(id))
	u.activate()

func show_radar(egg,id):
	#Check if exists first
	for u in $Player/Camera2D.get_children():
		if u.is_in_group("radar") and u.target == egg:
			u.enable()
			return
	
	var radar = radar_base.instance()
	$Player/Camera2D.add_child(radar)
	radar.target = egg
	radar.sprite_id = id
	radar.enable()
	

func hide_radar(egg):
	for u in $Player/Camera2D.get_children():
		if u.is_in_group("radar") and u.target == egg:
			u.disable()
			return



func _on_CameraCollision_body_entered(body):
	if body.is_in_group("egg"):# and body.state == body.s.STAY:
		hide_radar(body)


func _on_CameraCollision_body_exited(body):
	if body.is_in_group("egg"):# and body.state == body.s.STAY:
		show_radar(body,body.frame_id)

func first_egg():
	$Tutorial/Label4.show()


func _on_Area2D_body_entered(body):
	$Player.position = Vector2(0,-1300)
	$TileMap.queue_free()
	var t = load("res://Run.tscn").instance()
	get_tree().change_scene("res://Run.tscn")


func _on_Area2D2_body_entered(body):
	$Player.can_respawn = false
	$Music.stop()

func died():
	$Player.die()


func _on_mr_egg_died_body_entered(body):
	$Monster.died()
	$and_everyone_died_the_end.start()

func _on_and_everyone_died_the_end_timeout():
	get_tree().change_scene_to(load("res://Ending.tscn"))


func _on_stap_camera_body_entered(body):
	$Monster.stop_camera()
