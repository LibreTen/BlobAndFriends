extends KinematicBody2D

enum t {
	ONESHOT,
	TOGGLE
}

export(t) var type

export var start_active = false

export var speed = 40
export var offset = Vector2.ZERO
var active = false


onready var start_pos = position
onready var end_pos = position + offset

func activate():
	active = !active
#	if type == t.ONESHOT:
#		if !active:
#			pass
			#$AnimationPlayer.play("left_single")

func _physics_process(delta):
	var target : Vector2
	if active:
		target = end_pos
	else:
		target = start_pos
	
	if position.distance_to(target) > 1:
		var movement = (target-position).normalized() * speed
		position += movement*delta
