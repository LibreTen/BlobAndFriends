extends Area2D

enum t {
	ONESHOT,
	TEMPORARY
}

export(t) var type
export var id = 0

var body_count = 0


func _on_PressurePlate_body_entered(body):
	body_count += 1
	
	if body_count == 1:
		$press.play()
		find_parent("Level").activate(id)
		$Sprite.frame = 0


func _on_PressurePlate_body_exited(body):
	if type == t.TEMPORARY:
		body_count -= 1
	if body_count == 0 && type == t.TEMPORARY:
		$release.play()
		find_parent("Level").activate(id)
		$Sprite.frame = 1
