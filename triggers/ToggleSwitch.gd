extends StaticBody2D

export var id = 0

enum t {
	ONESHOT,
	TOGGLE
}

export(t) var type
var active = false
var a = true

func _ready():
	$AnimatedSprite.play()
	if type == t.TOGGLE:
		$AnimatedSprite.animation = "red"
	else:
		$AnimatedSprite.animation = "gray"

func trigger():
	if !a:
		return
	match $AnimatedSprite.animation:
		"gray":
			$AnimatedSprite.animation = "yellow"
		"red":
			$AnimatedSprite.animation = "blue"
		"blue":
			$AnimatedSprite.animation = "red"
	a = false
	
	var parent = find_parent("Level")
	if !active || type == t.TOGGLE:
		active = true
		$activate.play()
		$Timer.start()
		parent.call("activate",id)
	


func _on_Timer_timeout():
	a = true
