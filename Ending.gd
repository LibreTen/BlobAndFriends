extends Control

func _input(event):
	if event.is_action_pressed("ui_cancel") && $Sprite.frame == 1:
		get_tree().quit()
	elif event.is_action_pressed("ui_right") || event.is_action_pressed("right"):
		$Sprite.frame +=1
	elif event.is_action_pressed("ui_left") || event.is_action_pressed("left"):
		$Sprite.frame -= 1
	
	$Label.visible = $Sprite.frame == 0
	$Label2.visible = $Sprite.frame == 1
