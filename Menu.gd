extends Control

func _input(event):
	if event.is_action_pressed("ui_accept"):
		get_tree().change_scene_to(load("res://Intro.tscn"))
	elif event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	elif event.is_action_pressed("ui_cancel"):
		get_tree().quit()
